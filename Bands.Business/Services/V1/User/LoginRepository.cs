﻿
using Bands.Business;
using Bands.Common;
using Bands.Data;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Business
{
    public class LoginRepository : ILoginRepository
    {
        private readonly DBContext _context;
        public LoginRepository(DBContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

        }
        public User Authenticate(Login login)
        {
            return _context.User.Where(o => o.Username == login.Username && o.Password == login.Password).FirstOrDefault();
        }

        public string Generate(User user)
        {
         
                if (user != null)
                {
                    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("khanhkhanhkhanhkhanhkhanhkhanh12"));
                    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                    var claims = new[]
                    {
                new Claim(ClaimTypes.NameIdentifier,user.Username),
                new Claim(ClaimTypes.Role,user.Role) };
                    var token = new JwtSecurityToken("http://localhost:31410",
                        "http://localhost:31410",
                        claims,
                        expires: DateTime.Now.AddDays(1),
                        signingCredentials: credentials);
                    return new JwtSecurityTokenHandler().WriteToken(token);
                }
                return null;
          
        }
    }
}
