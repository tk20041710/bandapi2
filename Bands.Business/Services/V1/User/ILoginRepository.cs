﻿
using Bands.Business;
using Bands.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bands.Business
{
    public interface ILoginRepository
    {
        User Authenticate(Login login);
        string Generate(User user);
    }
}
