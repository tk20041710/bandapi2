﻿using Bands.Common;
using Bands.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bands.Business
{
    public class BandViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name needs to be filled in")]
        [MaxLength(100, ErrorMessage = "Name needs to be up to 100 characters")]
        public string Name { get; set; }

        public string FoundedYearsAgo { get; set; }
        [Required(ErrorMessage = "MainGenre needs to be filled in")]
        [MaxLength(50, ErrorMessage = "MainGenre needs to be up to 200 characters")]
        public string MainGenre { get; set; }
        public List<AlbumViewModel> Albums { get; set; }
    }


    public class BandsResourceParameters
    {
        public string MainGenre { get; set; }
        public string Filter { get; set; }
        public string Fields { get; set; }
        public string OrderBy { get; set; } = "Name";
    }


    public class BandCreatingUpdateModel
    {
        [Required(ErrorMessage = "Name needs to be filled in")]
        [MaxLength(100, ErrorMessage = "Name needs to be up to 100 characters")]
        public string Name { get; set; }

        [Required]
        public DateTime Founded { get; set; }

        [Required(ErrorMessage = "MainGenre needs to be filled in")]
        [MaxLength(50, ErrorMessage = "MainGenre needs to be up to 200 characters")]
        public string MainGenre { get; set; }


    }
    public class BandListModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string FoundedYearsAgo { get; set; }

        public string MainGenre { get; set; }
    }
    public class BandQueryModel : PaginationRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string? MainGenre { get; set; }
    }

}
