﻿using AutoMapper;
using Bands.Common;
using Bands.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Business
{
    public class BandHandler : IBandHandler
    {
        private readonly DBContext _dbcontext;
        private readonly IAlbumHandler _albumHandler;
        private readonly IMapper _mapper;
        public BandHandler(DBContext dbcontext, IMapper mapper, IAlbumHandler albumHandler)
        {
            _dbcontext = dbcontext ?? throw new ArgumentNullException(nameof(dbcontext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _albumHandler = albumHandler;

        }
        /// <summary>
        /// Tạo band
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<Response> CreateBand(BandCreatingUpdateModel param)
        {
            try
            {
                var band = await _dbcontext.Bands.Where(a => a.Name.ToLower() == param.Name.ToLower()).FirstOrDefaultAsync();
                if (band != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Band đã có !!");
                }

                band = new Data.Band();
                band.Name = param.Name;
                band.Founded = param.Founded;
                band.MainGenre = param.MainGenre;

               

                _dbcontext.Bands.Add(band);
                 await _dbcontext.SaveChangesAsync();
                var result = _mapper.Map<BandCreatingUpdateModel>(band);
                return new ResponseObject<BandCreatingUpdateModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}", param);
                return new Response<BandCreatingUpdateModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }
        /// <summary>
        /// Xóa band theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> DeleteBand(Guid id)
        {
            try
            {
                var band = await _dbcontext.Bands.Where(a => a.Id == id).FirstOrDefaultAsync();
                if (band == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Band không tồn tại!!");
                }

                _dbcontext.Bands.Remove(band);
                await _dbcontext.SaveChangesAsync();
                return new ResponseDelete(HttpStatusCode.OK, "Đã xóa thành công", id, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        /// <summary>
        /// Get band theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> GetById(Guid id)
        {
            try
            {
              
                var band = await  _dbcontext.Bands.Include(x => x.Albums).Where(a => a.Id == id).FirstOrDefaultAsync();
                if (band == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Band không tồn tại");
                }

                var result = _mapper.Map<Band, BandViewModel>(band);
                return new ResponseObject<BandViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@bandId}", id);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        /// <summary>
        /// Get band theo pagination
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<Response> GetPageAsync(BandQueryModel query)
        {
            try
            {
                

                var predicate = BuildQuery(query);

                
                var queryResult = _dbcontext.Bands.Include(x => x.Albums).Where(predicate);
                    
             
                var data = await queryResult.GetPageAsync(query);
                
                var result = _mapper.Map<Pagination<BandViewModel>>(data);
                

                

                if (result != null)
                {
                    return new ResponsePagination<BandViewModel>(result);
                }
                else
                return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        private Expression<Func<Band, bool>> BuildQuery(BandQueryModel query)
        {
           
            var predicate = PredicateBuilder.New<Band>(true);
            if (query.Filter != "{}")
            {
                var filter = query.Filter.Trim();
                predicate.And(b => b.Name.Contains(filter));
            }

            if (!string.IsNullOrWhiteSpace(query.MainGenre))
            {
                var fullTextSearch = query.MainGenre.Trim();
                predicate.And(b => b.MainGenre == fullTextSearch);
            }


            return predicate;
        }

        /// <summary>
        /// Sửa band theo id
        /// </summary>
        /// <param name="param"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Response> UpdateBand(BandCreatingUpdateModel param, Guid id)
        {
            try
            {
                var band = await _dbcontext.Bands.Where(a => a.Id == id).FirstOrDefaultAsync();

                if (band == null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Band không tồn tại !!!");
                }

                band.Name = param.Name;
                band.Founded = param.Founded;
                band.MainGenre = param.MainGenre;

            
                await _dbcontext.SaveChangesAsync();
                var result = _mapper.Map<Data.Band, BandCreatingUpdateModel>(band);
                return new ResponseObject<BandCreatingUpdateModel>(result);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@bandId}", id);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetBands(IEnumerable<Guid> ids)
        {
            try
            {
                var collection = await _dbcontext.Bands.Include(x => x.Albums).Where(a => ids.Contains(a.Id))
                                    .OrderBy(a => a.Name).ToListAsync();
                if(collection==null)
                {
                    return new Response(HttpStatusCode.BadRequest,"Không tồn tại");
                }    
                var result = _mapper.Map<IEnumerable<Band>,IEnumerable<BandViewModel>>(collection);
                return new ResponseObject<IEnumerable<BandViewModel>>(result);
               
            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
            
        }

        public async Task<Response> CreateBands(IEnumerable<BandCreatingUpdateModel> bands)
        {
           try
            {
                var bandEntities = _mapper.Map<IEnumerable<Band>>(bands);

               
                    _dbcontext.Bands.AddRange(bandEntities);
                    await _dbcontext.SaveChangesAsync();
                

                var result = _mapper.Map<IEnumerable<Band>, IEnumerable<BandViewModel>>(bandEntities);
                return new ResponseObject<IEnumerable<BandViewModel>>(result);
            } 
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}
