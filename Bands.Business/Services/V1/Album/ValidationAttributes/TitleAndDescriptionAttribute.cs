﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bands.Business
{
    public class TitleAndDescriptionAttribute : ValidationAttribute
    {
        /// <summary>
        /// validate của title và desciption
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var album = (AlbumCreatingModel)validationContext.ObjectInstance;

            if (album.Title == album.Description)
            {
                return new ValidationResult("The title and the description need to be different", new[] { "AlbumForCreatingDto" });
            }
         
            return ValidationResult.Success;
        }
    }
}