﻿using Bands.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Business
{
   public interface IAlbumHandler
    {
        Task<Response> CreateAlbum(AlbumCreatingModel param,Guid bandId);
        Task<Response> UpdateAlbum(AlbumUpdateModel param, Guid id,Guid bandId);
        Task<Response> DeleteAlbum(Guid id,Guid bandId);
        Task<Response> GetById(Guid bandId, Guid id);
        Task<Response> GetPageAsync(AlbumQueryModel query,Guid bandId);
    }
}
