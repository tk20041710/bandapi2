﻿using Bands.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bands.Business
{
    public class AlbumViewModel
    {
        public Guid Id { get; set; }


        public string Title { get; set; }

        public virtual string Description { get; set; }

        public Guid BandId { get; set; }
    }

    [TitleAndDescription(ErrorMessage = "Title Must Be Different From Description")]
    public class AlbumCreatingModel
    {

        [Required(ErrorMessage = "Title needs to be filled in")]
        [MaxLength(200, ErrorMessage = "Title needs to be up to 200 characters")]
        public string Title { get; set; }

        [MaxLength(400, ErrorMessage = "Description needs to be up to 400 characters")]
        public virtual string Description { get; set; }
    }


    public class AlbumUpdateModel
    {

        [Required(ErrorMessage = "Title needs to be filled in")]
        [MaxLength(200, ErrorMessage = "Title needs to be up to 200 characters")]
        public string Title { get; set; }

        [MaxLength(400, ErrorMessage = "Description needs to be up to 400 characters")]
        public virtual string Description { get; set; }
    }
    public class AlbumQueryModel : PaginationRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
