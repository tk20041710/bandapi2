﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bands.Data.Migrations
{
    public partial class abc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnDate",
                table: "Bands",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "Bands",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOnDate",
                table: "Albums",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOnDate",
                table: "Albums",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Albums",
                keyColumn: "Id",
                keyValue: new Guid("0e9a4ab5-4ae6-4ca3-ae7b-5f813e022527"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9916), new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9915) });

            migrationBuilder.UpdateData(
                table: "Albums",
                keyColumn: "Id",
                keyValue: new Guid("380c545c-9665-4043-baf2-34a3edefd373"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9911), new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9910) });

            migrationBuilder.UpdateData(
                table: "Albums",
                keyColumn: "Id",
                keyValue: new Guid("8d2744ff-1134-4f36-a300-043febdc64b8"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9921), new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9920) });

            migrationBuilder.UpdateData(
                table: "Albums",
                keyColumn: "Id",
                keyValue: new Guid("aaaccabe-29aa-42c4-9f80-18caea50adf5"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(8449), new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(8438) });

            migrationBuilder.UpdateData(
                table: "Albums",
                keyColumn: "Id",
                keyValue: new Guid("e5b6e8bf-5956-4329-a1b3-b1d48eea33ad"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9801), new DateTime(2022, 5, 26, 9, 45, 49, 653, DateTimeKind.Local).AddTicks(9795) });

            migrationBuilder.UpdateData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: new Guid("123eea43-5597-45a6-bdea-e68c60564247"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(6926), new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(542) });

            migrationBuilder.UpdateData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: new Guid("8e2f0a16-4c09-44c7-ba56-8dc62dfd792d"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8745), new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8745) });

            migrationBuilder.UpdateData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: new Guid("a052a63d-fa53-44d5-a197-83089818a676"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8692), new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8679) });

            migrationBuilder.UpdateData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: new Guid("cb554ed6-8fa7-4b8d-8d90-55cc6a3e0074"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8741), new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8739) });

            migrationBuilder.UpdateData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: new Guid("dab51058-0996-4221-ba63-b841004e89dd"),
                columns: new[] { "CreatedOnDate", "LastModifiedOnDate" },
                values: new object[] { new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8749), new DateTime(2022, 5, 26, 9, 45, 49, 652, DateTimeKind.Local).AddTicks(8748) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedOnDate",
                table: "Bands");

            migrationBuilder.DropColumn(
                name: "LastModifiedOnDate",
                table: "Bands");

            migrationBuilder.DropColumn(
                name: "CreatedOnDate",
                table: "Albums");

            migrationBuilder.DropColumn(
                name: "LastModifiedOnDate",
                table: "Albums");
        }
    }
}
