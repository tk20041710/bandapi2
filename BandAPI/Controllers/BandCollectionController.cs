﻿using AutoMapper;
using Bands.Services;
using Bands.Business;
using Bands.Common;
using Bands.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Bands.Controllers
{
    [Route("api/v1/bandcollections")]
    public class BandCollectionController : ControllerBase
    {
        private readonly IBandHandler _bandRepository;
        public BandCollectionController(IBandHandler bandRepository)
        {
            _bandRepository = bandRepository ??
          throw new ArgumentNullException(nameof(bandRepository));
        }
        /// <summary>
        /// Lấy list band
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet("({ids})", Name = "GetBandCollection")]
        public async Task< IActionResult> GetBandCollection([FromRoute]
               [ModelBinder(BinderType = typeof(ArrayModelBinder))]IEnumerable<Guid> ids)
        {         
            var bandEntities =await _bandRepository.GetBands(ids);

            return Helper.TransformData(bandEntities);

        }

        /// <summary>
        /// Thêm list band
        /// </summary>
        /// <param name="bandCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateBandCollection([FromBody] IEnumerable<BandCreatingUpdateModel> bandCollection)
        {
            var bands = await _bandRepository.CreateBands(bandCollection);
           return  Helper.TransformData(bands);

        }
    }
}
