﻿using AutoMapper;
using Bands.Business;
using Bands.Services;
using Bands.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Bands.Common;

namespace Bands.Controllers
{

    [ApiController]
    [Route("api/v1/bands/{bandId}/albums")]
    public class AlbumsController : ControllerBase
    {
        private readonly IAlbumHandler _albumRepository;

        public AlbumsController(IAlbumHandler albumRepository)
        {
            _albumRepository = albumRepository ??
          throw new ArgumentNullException(nameof(albumRepository));


        }

        /// <summary>
        /// Lấy album theo id
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAlbumForBand(Guid bandId, Guid id)
        {
            var result = await _albumRepository.GetById(bandId, id);
            return Helper.TransformData(result);

        }

        /// <summary>
        /// Lấy album thuộc bandId
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet(Name = "Getalbums")]
        public async Task<IActionResult> GetAlbumsForBand(Guid bandId, [FromQuery] int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "")
        {
            var filterObject = JsonConvert.DeserializeObject<AlbumQueryModel>(filter);
            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _albumRepository.GetPageAsync(filterObject, bandId);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm album
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAlbumForBand(Guid bandId, [FromBody] AlbumCreatingModel model)
        {
            var result = await _albumRepository.CreateAlbum(model, bandId);
            return Helper.TransformData(result);

        }


        /// <summary>
        /// Update album
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="Id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateAlbumForBand(Guid bandId, Guid Id, [FromBody] AlbumUpdateModel model)
        {
            var result = await _albumRepository.UpdateAlbum(model, Id, bandId);
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Xóa album
        /// </summary>
        /// <param name="bandId"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAlbumForBand(Guid bandId, Guid id)
        {
            var result = await _albumRepository.DeleteAlbum(id, bandId);
            return Helper.TransformData(result);
        }
    }
}
