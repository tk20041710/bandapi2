﻿using AutoMapper;

using Bands.Services;
using Bands.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;

using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bands.Controllers
{
    [ApiController]
    [Route("api/v1/login")]
    public class LoginController : ControllerBase
    {
        private readonly DbContext _context;
        private readonly ILoginRepository _loginRepository;
        public LoginController(DbContext context, ILoginRepository loginRepository)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _loginRepository = loginRepository;

        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody] Login login)
        {
            var user = _loginRepository.Authenticate(login);
            var token = _loginRepository.Generate(user);
                return Ok(token);
        }
    }
}
